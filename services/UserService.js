import Api from './Api'
import qs from 'qs'

const requestConfig = {
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
}

export default {
    getAll() {
        return Api().get('user')
    },

    changeStatus (id,data) {
        return Api().put(`/statusdeletedUser/${id}`,data)
    },

    deleteUser (id,data) {
        return Api().delete(`/user/${id}`, data)
    },

    requestResetPassword(email) {
        const emailQs = qs.stringify({email: email}, { indices: false })
        return Api().post('/password/email', emailQs, requestConfig);
    },

    ResetPassword (email, token , passwordData) {
        const passwordDataQs = qs.stringify(passwordData, { indices: false })
        return Api().post(`/password/reset/${token}/${email}`, passwordDataQs, requestConfig)
    }
}