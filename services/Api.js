import axios from 'axios'
import {state} from '../store'

export default () => {
    return axios.create({
        baseURL:'http://127.0.0.1:3333/',
        headers: {
            Authorization: `Bearer ${state().token}`
        }
    })
}