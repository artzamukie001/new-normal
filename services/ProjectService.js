import Api from './Api'
import qs from 'qs';

const requestConfig = {
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
}

export default {
    // Project Service
    getAll (){
        return Api().get('/projects')
    },
    getData () {
        return Api().get(`/project`)
    },
    addData (data) {
       return Api().post('/project', data)
   },
   updateData (id,data) {
    return Api().put(`/project/${id}`,data)
    },
    deleteData (id) {
        return Api().delete(`/project/${id}`)
    },

    userInProject (id) {
        return Api().get(`/user/project/${id}`)
    },

    // Feature Service
    getAllFeature(id) {
        return Api().get(`/project/${id}/feature`)
    },
    addFeature (feature) {
        return Api().post(`/feature`, feature)
    },
    updateFeature (id,feature) {
     return Api().put(`/feature/${id}`,feature)
     },
    changeStatusFeature (id, data) {
        return Api().put(`/statusFeature/${id}`,data)
    },
    deleteFeature (id) {
        return Api().delete(`/feature/${id}`)
    },

    // Test Case Service
    getAllTestcase(project_id, id){
        return Api().get(`/project/${project_id}/feature/${id}/testcase`)
    },
    addTestcase(testCase){
        const testcaseQs = qs.stringify(testCase);
        return Api().post(`/testcase`,testcaseQs,requestConfig)
    },
    updateTestcase (id,testCase) {
        return Api().put(`/testcase/${id}`,testCase)
    },
    deleteTestcase (id) {
        return Api().delete(`/testcase/${id}`)
    },

    //TEP
    getAllTep(id) {
        return Api().get(`/project/${id}/teps`)
    },
    addTep(id,tep) {
        const config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }
        return Api().post(`/project/${id}/tep`,tep,config)
    },
    updateTep (projectId,id,tep) {
        return Api().put(`/project/${projectId}/tep/${id}`,tep)
    },
    deleteTep (projectId,id) {
        return Api().delete(`/project/${projectId}/tep/${id}`)
    },

    //Feature in TEP
    getAllFeatureListinTEP(projectId,id) {
        return Api().get(`/project/${projectId}/TEP/${id}/listfeature`)
    },

    getAllfeatureinTEP(projectId,id) {
        return Api().get(`/project/${projectId}/TEP/${id}/feature`)
    },
    addFeatureinTeP(projectId,id,feature){
        const featureQs = qs.stringify(feature);
        return Api().post(`/project/${projectId}/TEP/${id}/feature`,featureQs,requestConfig)
    },
    deleteFeatureinTEP(projectId,tep_id,id) {
        return Api().delete(`/project/${projectId}/TEP/${tep_id}/feature/${id}`)
    },

    //testcase in TEP
    getAllTestcaseToTep(project_id,tep_id,id){
        return Api().get(`/project/${project_id}/TEP/${tep_id}/feature/${id}/testcase`)
    },
    addTestcaseToFeature(project_id,id,tep_id,TestCase){
        const testcaseQs = qs.stringify(TestCase, { indices: false })
        return Api().post(`/project/${project_id}/TEP/${tep_id}/feature/${id}/testcase`,testcaseQs,requestConfig)
    },
    updateTCinTEP(projectId,tep_id,feature_id,id) {
        return Api().put(`/project/${projectId}/TEP/${tep_id}/feature/${feature_id}/testcase/${id}`)
    },
    deleteTCinTEP(projectId,tep_id,feature_id,id) {
        return Api().delete(`/project/${projectId}/TEP/${tep_id}/feature/${feature_id}/testcase/${id}`)
    },

    //Priority
    getAllPriority(){
        return Api().get(`/priority`)
    },
    addPriority(data){
        return Api().post(`/priority`,data)
    },
    updatePriority (id,data) {
        return Api().put(`/priority/${id}`,data)
        },
    deletePriority (id) {
            return Api().delete(`/priority/${id}`)
        },

    //Servity
    getAllServity(){
        return Api().get(`/servity`)
    },
    addServity(data){
        return Api().post(`/servity`,data)
    },
    updateServity (id,data) {
        return Api().put(`/servity/${id}`,data)
        },
    deleteServity (id) {
            return Api().delete(`/servity/${id}`)
        },

    //Defect Status
    getAllDefect(){
        return Api().get(`/defectStatus`)
    },
    addDefect(data){
        return Api().post(`/defectStatus`,data)
    },
    updateDefect (id,data) {
        return Api().put(`/defectStatus/${id}`,data)
        },
    deleteDefect (id) {
            return Api().delete(`/defectStatus/${id}`)
        },
    
    //Test Result 
    getTestResult(){
        return Api().get(`/testresult`)
    },
    addTestResult(){
        return Api().post(`/testresult`)
    }
}