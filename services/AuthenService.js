import Api from './Api'
import LoginApi from './LoginApi'

export default {
    register(credentials) {
        return Api().post(`/auth/register`,credentials)
    },

    login(credentials) {
        return LoginApi().post(`login`,credentials)
    }
}