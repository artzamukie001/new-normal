import AuthenService from '@/services/AuthenService'
import isLoggedIn from '../middleware/isLoggedIn'

export const state = () => {
    return{
        token:localStorage.getItem('access_token'),
        user: JSON.parse(localStorage.getItem('user')),
        isLoggedIn:false
    }
}

export const mutations = {
    auth_success (state, token, user){
        state.token = token
        state.user = user
        state.isLoggedIn = true
    },
    // auth_error (state){
    //     state.isLoggedIn = false;
    // },

    setToken (state,token){
        console.log('how to keep token');
        
        localStorage.setItem('access_token', token)
        state.isLoggedIn = !!(token)
    },
    setUser (state,user){
        state.user = user
    }
}

export const actions = {
    login({commit},user){
        return new Promise((resolve, reject) => {
            AuthenService.login(user)
            .then(resp => {
                const token = resp.data.access_token.token
                const user = resp.data.user
                localStorage.setItem('access_token', token)
                localStorage.setItem('user', JSON.stringify(user));
                commit('auth_success',token,user)
                resolve(resp)
            })
            .catch(err => {
                console.error(err);
                //commit('auth_error', err)
                localStorage.removeItem('access_token')
                reject(err)
            })
        })
    },

    setToken ({commit},token){
        commit('setToken',token)
        console.log('akjf;lasjdf');
        
    },
    setUser ({commit},user){
        commit('setUser',user)
    }
   
}
export const getters = {
    isLoggedIn: state => !!state.token,
    user: state => state.user
}

